class Bullet {
  constructor(x) {
    this.w = 10;
    this.h = this.w * 1.75;
    this.d = 12;
    this.x = x;
    this.y = height - this.h - 34;
    this.speed = 10;
    this.delete = false;
  }

  update() {
    this.y -= this.speed;
  }

  draw() {
    noStroke();
    fill(110, 110, 110);
    if (this.y >= -this.d / 6)
      circle(this.x, this.y, this.d);
    else
      this.delete = true;
    // rect(this.x, this.y, this.w, this.h);
  }
}