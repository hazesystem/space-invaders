class Invader {
  constructor(x, y) {
    this.w = 36;
    this.h = 36;
    this.x = x;
    this.y = y;
    this.speed = 2;
    this.delete = false;
  }

  draw() {
    noStroke();
    fill(255);
    rect(this.x, this.y, this.w, this.h);
  }
}