var ship;
var bullets = [];
var invaders = [];
// var p;

function setup() {
  frameRate(30);
  createCanvas(400, 400);
  // p = createP("");
  ship = new Ship();

  for (let x = 1; x <= 5; x++) {
    let m = x * ((width - 5 * 36) / 6);
    console.log(m);
    invaders.push(new Invader(m + (x - 0.6) * 36, 50));
  }
}

function draw() {
  background(0);

  if (keyIsDown(RIGHT_ARROW))
    ship.move(1);
  else if (keyIsDown(LEFT_ARROW))
    ship.move(-1);

  ship.draw();

  for (let i = bullets.length - 1; i >= 0; i--) {
    if (bullets[i].delete)
      bullets.splice(i, 1);
    else {
      bullets[i].update();
      bullets[i].draw();
    }
  }

  for (inv of invaders) {
    inv.draw();
  }
}

function keyPressed() {
  if (key === ' ')
    bullets.push(new Bullet(ship.x));
}