class Ship {
  constructor() {
    this.w = 36;
    this.h = 36;
    this.x = width / 2;//- this.w / 2;
    this.y = height - this.h / 1.5;
    this.speed = 5;
  }

  move(d) {
    this.x += d * this.speed;
  }

  draw() {
    noStroke();
    fill(100, 0, 200);
    rectMode(CENTER);
    rect(this.x, this.y, this.w, this.h);
  }
}